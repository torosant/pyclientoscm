# OSCM Client for Python
The OSCM Client is the Python client to easily submit and request transactions to/from OSCM.

# Installation

```
pip install oscm_client
```

### For Developers
```
git clone https://gitlab.com/torosant/pyclientoscm.git
cd PyClientOSCM
pip install -e .
```

# Documentation and examples
TBC


# Requirements
* The OSCM Client requires Python 3.4 or greater.


# Support
TBC
