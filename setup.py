from setuptools import setup

setup(
    name='oscm_client',
    version='0.0.1',
    packages=['oscm_client'],
    description='OSCM Client for python',
    long_description=("The OSCM Client is the Python client to easily submit and request"
                      " transactions to/from OSCM."),
    install_requires=[
        "requests>=2.22.0"
    ],
    python_requires=">=3.4",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: Apache Software License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.4",
    ],
    keywords=[
        "OSCM",
        "Operating System for ciber-physical manufacturing",
        "network of resources",
        "OSCM Client Python",
        "OSCM Client"
    ],
    license="Apache License, Version 2.0",
    url="https://gitlab.com/torosant/pyclientoscm.git",
    author="Ricardo Toro",
    author_email="torosant@illinois.edu"
)