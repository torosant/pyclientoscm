# https://gitignore.io/         crete a gitignore file
# https://choosealicense.com/   Choose license app

import requests     # package to deal with http
import json         # package to convert dict to json
import os           # package to get path
import cgi          # package to parse headers

# server locators
dev_server = "http://localhost:8001"
prod_server = "https://oscm-il.mechse.illinois.edu"

# Basic general routes
users_route = "/users"
internal_route = "/internal"
transactions_route = "/transactions"
noam_resources_route = "/noam-resources"
noam_users_route = "/noam-users"
noam_emails_route = "/noam-emails"
noam_route = "/noam"
account_events_route = "/account-events"


class OSCMClient:
    '''
    This class offers a basic set of HTTP requests to OSCM server (service).
    It can be initialized for a ``dev`` or ``prod`` OSCM server.

    Arguments:
        server_instance (str): default: ``prod`` -> uses live oscm server (https://oscm-il.mechse.illinois.edu),
                                        ``dev``  -> uses developer server locator (http://localhost:8001)
    '''

    # Constructor
    def __init__(self, server_instance="prod"):
        '''
        Initialize OSCM client
        Public members:
            token: JWT token.
            user: (User profile dict)
                profile:
                    id:
                    name:
                    username:
                    email:
                    address:
                        geo:
                            [lat, lng]
                        country:
                        postal_code:
                        state:
                        city:
                        street_address:
                    type:
            is_auth: Let us know if user is authenticated with oscm (True -> authenticated, False -> Not authenticated)

        Arguments:
            server_instance (str): default: ``prod`` -> uses live oscm server (https://oscm-il.mechse.illinois.edu),
                                            ``dev``  -> uses developer server locator (http://localhost:8001)

        Returns:
            OSCMClient: An initialized OSCM Client.
        '''

        # set server locator
        if server_instance == "prod":
            self.server_loc = prod_server
        elif server_instance == "dev":
            self.server_loc = dev_server
        else:
            self.server_loc = server_instance

        # Init public members
        self.token = None
        self.user = {}
        self.is_auth = False

    # ---------------------------------------------------
    # Authenticate:
    # ---------------------------------------------------

    def authenticate(self, username=None, password=None):
        '''
        Authenticate user into OSCM server.
        Public members:
            token: JWT token.
            user: (User profile dict)
                profile:
                    id:
                    name:
                    username:
                    email:
                    address:
                        geo:
                            [lat, lng]
                        country:
                        postal_code:
                        state:
                        city:
                        street_address:
                    type:
            is_auth: Let us know if user is authenticated with oscm (True -> authenticated, False -> Not authenticated)

        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info,
            _error (str): Error message, if applicable,

        Arguments:
            username (str): defaul: None,
            password (str): default: None

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
        '''

        # verify we have credentials required for this instance
        if username is None and password is None:
            raise TypeError(
                "'username' and 'password' are required arguments.")
        if username is None:
            raise TypeError("'username' is a required argument.")
        if password is None:
            raise TypeError("'password' is a required argument.")

        payload = {"username": username, "password": password}

        # Make the request
        try:
            res = requests.post(self.server_loc + users_route +
                                '/authenticate',  json=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    self.token = json_res["token"]
                    self.user = json_res["user"]
                    self.is_auth = True
                else:
                    self.is_auth = False
                    raise ValueError(
                        "Unable to authenticate. Wrong username or password.")
            else:
                self.is_auth = False
                raise ValueError("Unable to authenticate. {}, Status Code: {}".format(
                    _error, res.status_code))

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
        }

    # ---------------------------------------------------
    # Getters:
    # ---------------------------------------------------

    # Users:
    def get_profile(self, path="_doc"):
        '''
        Get user profile info from OSCM server. If path=``_doc``, the method returns the complete user model.
        Otherwise, path defines wich part of the model to return. The full model is described below in _field private member.
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info,
            _error (str): Error message, if applicable,
            _field (dict): User model. Depends on the path as explained above:
                profile:
                    _id:
                    name:
                    username:
                    email:
                    address:
                        geo:
                            [lat, lng]
                        country:
                        postal_code:
                        state:
                        city:
                        street_address:
                    type:
                    phone:
                    password: (hashed)
                resources: [
                    process_name:
                    user_type:
                    name:
                    _id:
                    camera_capable:
                ]
                facilities: [
                    user_type:
                    facility_name:
                    _id:
                ]
                accounts: [
                    facility: (optional)
                    type:
                    number:
                    _id:
                ]
                email_confirmed:

        Arguments:
            path (str): defaul: ``_doc``. Depending on this path, the output field returns the desired part of the model.
                ex. path="facilities" returns a list of all facilities registered under the user account with fields:
                user_type, facility_name, and _id.

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                field (dict): _field described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(self.server_loc + users_route +
                               '/user/profile/' + path,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _field = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Server may be experiencing technical difficulties.").format(
                    res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    if 'field' in json_res.keys():
                        _success = True
                        _field = json_res['field']
                    else:
                        raise ValueError("'path' does not exist in user model")
                else:
                    _success = False
                    _error = "Failed to get user profile"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "Failed to get user profile"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "field": _field
        }

    # get any user by id:
    def get_user_by_id(self, _id=None):
        '''
        Get any user profile info from OSCM server given an id.
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info,
            _error (str): Error message, if applicable,
            _user (dict): User model.
                profile:
                    _id:
                    name:
                    username:
                    email:
                    address:
                        geo:
                            [lat, lng]
                        country:
                        postal_code:
                        state:
                        city:
                        street_address:
                    type:
                    phone:
                    password: (hashed)
                resources: [
                    process_name:
                    user_type:
                    name:
                    _id:
                    camera_capable:
                ]
                facilities: [
                    user_type:
                    facility_name:
                    _id:
                ]
                accounts: [
                    facility: (optional)
                    type:
                    number:
                    _id:
                ]
                email_confirmed:

        Arguments:
            id (str): defaul: None. id of the user

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                user (dict): _user described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify id is given
        if _id is None:
            raise TypeError("'_id' is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(
                self.server_loc + noam_users_route + '/get/' + _id,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _user = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _user = json_res['user']
                else:
                    _success = False
                    _error = "failed to get user"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get user"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "user": _user
        }

    # Resources:
    def get_resource(self, _id=None):
        '''
        Get one resource from OSCM server defined by the _id provided in the parameter ``_id``. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _resource (dict):  The dict of the requested resource:
                _id: 
                owner:
                    _id:
                    name:
                configuration:
                    profile:
                        oscmkey: 
                        license_number:
                        resourcename:
                        camera_capable:
                        picture:
                        description:
                    communications:
                        adapters: [
                            locator:
                                address:
                                port:
                                octopi_address:
                            configuration:
                                name:
                                id:
                                adapter_type:
                                version:
                                stream_type:
                                stream_category:
                                active:
                        ]
                        OSCM_cloud:
                            locator:
                                address:
                                ip:
                                port:
                        freq_updates:
                    file:
                        oscm:
                    calendar:
                    form: (optional)
                address:
                    country:
                    postal_code:
                    state:
                    city:
                    street_address:
                geo: [lat, lng]
                users: [
                    _id:
                    name:
                    user_type:
                    account:
                        number:
                        type:
                ]

        Arguments:
            _id (str): defaul: None. Id of the resource that we want to get from OSCM server.

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                resource (dict): _resource described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify id is given
        if _id is None:
            raise TypeError("'_id' is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(
                self.server_loc + noam_resources_route + '/getConfiguration/' + _id,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _resource = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _resource = json_res['resource']
                else:
                    _success = False
                    _error = "failed to get resource"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get resource"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "resource": _resource
        }

    # Facility:
    def get_facility(self, _id=None, path='_doc'):
        '''
        Get one facility from OSCM server defined by the _id provided in the parameter ``_id``. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _facility (dict):  The dict of the requested facility:
                _id: 
                owner_locator:
                    _id:
                    name:
                configuration:
                    profile:
                        oscmkey: 
                        license_number:
                        facility_name:
                        camera_capable:
                        picture:
                        description:
                    communications:
                        OSCM_cloud:
                            locator:
                                address:
                                ip:
                                port:
                    calendar:
                    queues:
                        name:
                        description:
                    form: (optional)
                physical_locator:
                    address:
                        address_string:
                        country:
                        postal_code:
                        state:
                        city:
                        street_address:
                    location_data:
                        geo: [lat, lng]
                users_locator: [
                    _id:
                    name:
                    user_type:
                    account:
                        number:
                        type:
                ]
                active:

        Arguments:
            _id (str): defaul: None. Id of the facility that we want to get from OSCM server.
            path (str): defaul: ``_doc`` => all facility model, otherwise need to specify the desired path

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                facility (dict): _facility described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify id is given
        if _id is None:
            raise TypeError("'_id' is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(
                self.server_loc + users_route + '/facility/get/' + _id + '/' + path,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _facility = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    if 'facility' in json_res.keys():
                        _success = True
                        _facility = json_res['facility']
                    else:
                        raise ValueError(
                            "'path' does not exist in facility model")
                else:
                    _success = False
                    _error = "failed to get facility"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get facility"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "facility": _facility
        }

    # Transactions:
    def get_transactions(self, role="customer", version="light"):
        '''
        Get user's transactions from OSCM server. This is a general service that depends on the role and version.
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _transactions (list of dicts):  List of transacions that the user has. The get request uses the role 
                parameter to verify if we are requesting transactions as a "customer" or as a "provider". Also, the
                version parameter defines the transaction dict variable as:

                for version = "light":
                    id:
                    name:
                    status:
                    provider_name:
                    resource_name:
                    queue:
                    payment_method:
                    submitted:

                for version = "full":
                    _id:
                    messages: []
                    properties:
                        name:
                        account: (optional)
                        payment_method: (optional)
                        cost: (optional)
                        submitted:
                        status
                    job_locator:
                        _id:
                    resource_locator:
                        _id:
                        name:
                        queue: (optional)
                        type:
                    customer_locator:
                        _id:
                        name:
                        email:
                        phone:
                    provider_locator:
                        _id:
                        name:
                        email:

        Arguments:
            role (str): defaul: ``customer``. Depending on this role, the transactions returned are from the
                ``customer`` or ``provider`` perspective.
            version (str): default: ``light``. Both ``light`` and ``full`` versions are explained above in the _transactions list.
                Depending on the value of this parameter, the _transactions list outputs different dict for transaction model. 

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                transactions (list): _transactions described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(self.server_loc + transactions_route +
                               '/get/' + role + '/' + version,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _transactions = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if version == "full" and json_res['success']:
                    _success = True
                    _transactions = json_res['transactions']
                elif version == "light":
                    _success = True
                    _transactions = json_res
                else:
                    _success = False
                    _error = "failed to get transactions"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get transactions"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "transactions": _transactions
        }

    def get_transaction(self, _id=None):
        '''
        Get one transaction from OSCM server defined by the _id provided in the parameter ``_id``. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _transaction (dict):  The dict of the requested transaction:
                _id:
                messages: []
                properties:
                    name:
                    account: (optional)
                    payment_method: (optional)
                    cost: (optional)
                    submitted:
                    status
                job_locator:
                    _id:
                resource_locator:
                    _id:
                    name:
                    queue: (optional)
                    type:
                customer_locator:
                    _id:
                    name:
                    email:
                    phone:
                provider_locator:
                    _id:
                    name:
                    email:
            _job (dict): The job that is attached to the requested transaction:
                _id:
                other: (dict) -> depends on the type of job
                instructions: (optional)
                quantity: (optional)
                times:
                    processing: (optional)
                    cleaning: (optional)
                dates:
                    duedate: (optional)
                    readytime: (optional)
                    submitted:
                    end: (optional)
                    start: (optional)
                properties:
                    locked:
                    mode:
                    type:
                    status:
                users_locator:
                    customer_id:
                    resource_id:
                calendar_locator:
                    _id:   

        Arguments:
            _id (str): defaul: None. Id of the transaction that we want to get from OSCM server.

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                transaction (dict): _transaction described in private member (see above)
                job (dict): _job described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify id is given
        if _id is None:
            raise TypeError("'_id' is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(
                self.server_loc + transactions_route + '/get/' + _id,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _transaction = None
        _job = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _transaction = json_res['transaction']
                    _job = json_res['job']
                else:
                    _success = False
                    _error = "failed to get transaction"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get transaction"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "transaction": _transaction,
            "job": _job
        }

    # Files
    def get_files_metadata(self, _id=None):
        '''
        Get all metadata from the files that belong to a transaction from the OSCM server. The transaction is defined by the its ``_id``. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _files (list of dict):  List of metadata file: 
                [ 
                    {
                        id:
                        filename:
                        contentType:
                        metadata: {
                            user_id:
                            transaction:
                            provider:
                        }
                    }
                ]

        Arguments:
            _id (str): defaul: None. Id of the transaction that we want to get metadata files from.

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                files (dict): _files described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify id is given
        if _id is None:
            raise TypeError("'_id' is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(
                self.server_loc + transactions_route + '/files/' + _id,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _files = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _files = json_res['files']
                else:
                    _success = False
                    _error = "failed to get file metadata"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get file metadata"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "files": _files
        }

    def get_file(self, _id=None):
        '''
        Get a file from OSCM server. The required file is located by its ``_id``. The file is temporarily stored in os.path/oscm_files directory.
        All files are removed from the directory when OSCMClient destructor (deleting) is called. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _filename (str):  name of the requested file 

        Arguments:
            _id (str): defaul: None. Id of the file that we want get from OSCM server.

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                filename (str): _filename described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify id is given
        if _id is None:
            raise TypeError("'_id' is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Make the request
        try:
            res = requests.get(
                self.server_loc + transactions_route + '/file/download/' + _id,  headers=headers)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _filename = None
        try:
            # get filename
            value, params = cgi.parse_header(
                res.headers['Content-Disposition'])
            _filename = params['filename']
            # use oscm_files folder to save file
            oscm_path = os.path.abspath('oscm_files')
            # create and save content into file:
            with open(os.path.join(oscm_path, _filename), 'wb') as f:
                for chunk in res:
                    f.write(chunk)
                f.close()
        except Exception:
            if res.status_code < 300:
                _error = "failed to get file"
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if res.status_code == 200:
                    _success = True
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to get file"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "filename": _filename
        }
    # ---------------------------------------------------
    # Setters:
    # ---------------------------------------------------

    # Users (NO Auth required!):
    def register_new_user(self, user=None):
        '''
        Create a new user in OSCM server. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _user (str):  The id of the registered user:

        Arguments:
            user (dic): defaul: None. user info:
                profile:
                    _id:
                    name:
                    username:
                    email:
                    address:
                        geo:
                            [lat, lng]
                        country:
                        postal_code:
                        state:
                        city:
                        street_address:
                    type:
                    phone:
                    password: (hashed)
                accounts: []

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                user_id (str): user id

        Note: NO Auth required!
        '''

        # Verify user is given
        if user is None:
            raise TypeError("user is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json'}

        # Set payload
        payload = user

        # Make the request
        try:
            res = requests.post(
                self.server_loc + internal_route + '/users/register',  headers=headers, json=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _user = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _user = json_res['user_id']
                else:
                    _success = False
                    _error = "failed to register user"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to register user"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "user": _user,
        }

    def add_event(self, current_event=None):
        '''
        Add an event in OSCM server. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _event (str):  The id of the registered user:

        Arguments:
            event (dic): defaul: None. event info:
                event_type:
                user:
                user_email:
                _id:
                resolved:
                created:

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                event (str): _event described in private member (see above)

        Note: NO Auth required!
        '''

        # Verify current_event is given
        if current_event is None:
            raise TypeError("current_event is a required argument.")

        # Define header
        headers = {'Content-Type': 'application/json'}

        # Set payload
        payload = {"event": current_event}

        # Make the request
        try:
            res = requests.post(
                self.server_loc + account_events_route + '/createEvent',  headers=headers, json=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _event = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _event = json_res['event']
                else:
                    _success = False
                    _error = "failed to set event"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to set event"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "event": _event,
        }

    def send_confirmation_email(self, event_id=None, email=None):
        '''
        Send confirmation email after register user in OSCM. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable. 

        Arguments:
            event_id (str): defaul: None. event id
            email (str): defaul: None. email address

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server

        Note: NO Auth required!
        '''

        # Verify all parameters are given
        if event_id is None or email is None:
            raise TypeError("event_id and email are required arguments.")

        # Define header
        headers = {'Content-Type': 'application/json'}

        # Set payload
        payload = {"receiver": email, "event_id": event_id}

        # Make the request
        try:
            res = requests.post(
                self.server_loc + noam_emails_route + '/confirm',  headers=headers, json=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                else:
                    _success = False
                    _error = "failed to send email"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to send email"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
        }

    # Transaction
    def submit_transaction(self, job=None, transaction=None):
        '''
        Create a transaction in OSCM server. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _transaction (dict): The dict of the requested transaction:
                _id:
                messages: []
                properties:
                    name:
                    account: (optional)
                    payment_method: (optional)
                    cost: (optional)
                    submitted: (optional)
                    status
                job_locator:
                    _id:
                resource_locator:
                    _id:
                    name:
                    queue: (optional)
                    type:
                customer_locator:
                    _id:
                    name:
                    email:
                    phone:
                provider_locator:
                    _id:
                    name:
                    email:

                _id:
                other: (dict) -> depends on the type of job
                instructions: (optional)
                quantity: (optional)
                times:
                    processing: (optional)
                    cleaning: (optional)
                dates:
                    duedate: (optional)
                    readytime: (optional)
                    submitted:
                    end: (optional)
                    start: (optional)
                properties:
                    locked:
                    mode:
                    type:
                    status:
                users_locator:
                    customer_id:
                    resource_id:
                calendar_locator:
                    _id:   

        Arguments:
            job (dic): defaul: None. Job that user want to request.
            transaction (dic): defaul: None. Basic transaction info required to request transaction

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                transaction (dict): _transaction described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify job and transaction are given
        if job is None or transaction is None:
            raise TypeError("job and transaction are required arguments.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Set payload
        payload = {"transaction": transaction, "job": job}

        # Make the request
        try:
            res = requests.post(
                self.server_loc + transactions_route + '/configure',  headers=headers, json=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _transaction = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = "failed to submit transaction"
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _transaction = json_res['transaction']
                else:
                    _success = False
                    _error = "failed to submit transaction"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to submit transaction"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "transaction": _transaction,
        }

    def send_email(self, receiver=None, subject=None, body=None):
        '''
        Send an email to an OSCM user. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable. 

        Arguments:
            receiver (str): defaul: None. Email address of the receiver
            subject (str): defaul: None. The subject of the email
            body (str): defaul: None. The body of the email

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify all parameters are given
        if receiver is None or subject is None or body is None:
            raise TypeError(
                "receiver, subject and body are required arguments.")

        # Define header
        headers = {'Content-Type': 'application/json',
                   'Authorization': self.token}

        # Set payload
        payload = {"receiver": receiver, "subject": subject, "body": body}

        # Make the request
        try:
            res = requests.post(
                self.server_loc + noam_emails_route + '/sendEmail',  headers=headers, json=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                else:
                    _success = False
                    _error = "failed to send email"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to send email"

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
        }

    # Files
    def submit_file(self, transaction=None, path=None, filename=None):
        '''
        Submit a file to an existing OSCM transaction. 
        Private members:
            _success (Bool): Default False. True -> get desired response, False -> Failed to retreive info
            _error (str): Error message, if applicable.
            _filename (str): filename
            _file_id (str): file id

        Arguments:
            transaction (str): defaul: None. oscm transaction
            path (str): defaul: None. Path where the file is stored (localy)
            filename (str): defaul: None. Name of the file to be submitted

        Returns:
            *dict*
                success (Bool): True -> desired response, False -> Failed to retreive info
                error (str): Error message, if applicable.
                status_code (int): response code from server
                filename (str): _filename described in private member (see above)
                file_id (str): _file_id described in private member (see above)
        '''

        # Verify user is authenticated
        if self.is_auth is False:
            raise ValueError("'token' expired or user never authenticated")

        # Verify job and transaction are given
        if transaction is None or path is None or filename is None:
            raise TypeError(
                "transaction, path and filename are required arguments.")

        # Verify transaction is a dict
        if type(transaction) is not dict:
            raise TypeError("Transaction argument must be a dict.")

        # retreive required info for the request:
        if 'user_id' in transaction.keys() and 'transaction_id' in transaction.keys() and 'provider_id' in transaction.keys():
            user_id = transaction['user_id']
            transaction_id = transaction['transaction_id']
            provider_id = transaction['provider_id']
        else:
            raise TypeError("Please check the required arguments.")

        # Set file
        files = {'file': open(os.path.join(path, filename), 'rb')}

        # Set payload
        payload = {'current_user': user_id, 'transaction': transaction_id,
                   'transaction_provider': provider_id}

        # Define header
        headers = {'Authorization': self.token}

        # Make the request
        try:
            res = requests.post(
                self.server_loc + noam_route + '/file/upload', headers=headers, files=files, data=payload)

        except requests.exceptions.RequestException as e:
            raise ConnectionError("OSCM Server is currently down")

        # Check for success
        _success = False
        _error = None
        _filename = None
        _file_id = None
        try:
            json_res = res.json()
        except Exception:
            if res.status_code < 300:
                _error = "Error decoding {} response: {}".format(
                    res.status_code, res.content)
            else:
                _error = ("Error {}. OSCM Connect may be experiencing technical"
                          " difficulties.").format(res.status_code)
        else:
            # Prepare the output
            if res.status_code < 300:
                if json_res['success']:
                    _success = True
                    _filename = json_res['filename']
                    _file_id = json_res['file_id']
                else:
                    _success = False
                    _error = "failed to submit file"
            elif res.status_code == 401:
                self.is_auth = False
                _error = ("Error {}. 'token' expired or user never authenticated").format(
                    res.status_code)
            else:
                _success = False
                _error = "failed to submit file"

        files['file'].close()  # Im not sure we need this, but just in case

        # Return results
        return {
            "success": _success,
            "error": _error,
            "status_code": res.status_code,
            "filename": _filename,
            "file_id": _file_id
        }

    # Deleting (Calling destructor)
    def __del__(self):

        # remove all files from oscm_files dir
        oscm_path = os.path.abspath('oscm_files')
        filelist = [f for f in os.listdir(oscm_path)]
        for f in filelist:
            os.remove(os.path.join(oscm_path, f))
