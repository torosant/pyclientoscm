from oscm_client import OSCMClient
import datetime, time
import os
import json
import pytest

# good parameters
server_instance = 'dev'
username = 'test'
password = 'Test123*'

# ---------------------------------------------------
# Session:
# ---------------------------------------------------
''' def test_server_down():
    session = OSCMClient(server_instance)

    # Server down:
    with pytest.raises(ConnectionError) as e:
        session.authenticate(username, password)
    assert str(e.value) == 'OSCM Server is currently down' '''
 
def test_authenticate():
    session = OSCMClient(server_instance)
    assert session.token == None
    assert session.user == {}
    assert session.is_auth == False

    # Corret credentials:
    session.authenticate(username, password)
    assert session.is_auth == True
    assert session.user == {
        'profile': {
            'id': '5cf5a5765785504c8899cd29',
            'name': 'Test',
            'username': 'test',
            'email': 'torosant@illinois.edu',
            'address': {
                'geo': [40.1111367, -88.2250555],
                'country': 'United States',
                'postal_code': '61801',
                'state': 'IL',
                'city': 'Urbana',
                'street_address': '1206 West Green Street'
            },
            'type': 'user'
        }
    }

    # Wrong credentials:
    with pytest.raises(ValueError) as e:
        session.authenticate(username, 'password')
    assert str(e.value) == 'Unable to authenticate. Wrong username or password.'

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.authenticate(username)
    assert str(e.value) == "'password' is a required argument."

    with pytest.raises(TypeError) as e:
        session.authenticate(password='password')
    assert str(e.value) == "'username' is a required argument."

    with pytest.raises(TypeError) as e:
        session.authenticate()
    assert str(e.value) == "'username' and 'password' are required arguments."

# ---------------------------------------------------
# Users:
# ---------------------------------------------------

def test_get_profile():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_profile("profile.phone")
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # test phone
    response = session.get_profile("profile.phone")
    assert response['success'] == True
    assert response['field'] == '(217) 600-9690'

    # test resources
    response = session.get_profile('resources')
    assert response['success'] == True
    assert response['field'] == [{
        'process_name': 'Graphene Synthesis',
        'user_type': 'guest',
        'name': 'Test CVD',
        '_id': '5cf5a94f5785504c8899cd66',
        'camera_capable': True
    }]

    # test facilities
    response = session.get_profile('facilities')
    assert response['success'] == True
    assert response['field'] == [{
        'facility_name': 'CVDs Facility Test',
        'user_type': 'guest',
        '_id': '5cf5aa095785504c8899cd7c'
    }]

    # wrong user input:
    with pytest.raises(ValueError) as e:
        session.get_profile("anything")
    assert str(e.value) == "'path' does not exist in user model"

def test_get_user_by_id():

    session = OSCMClient(server_instance)
    
    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_profile("profile.phone")
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # user id
    _id = '5cf5a5765785504c8899cd29'
    response = session.get_user_by_id(_id)
    assert response['success'] == True
    assert response['user'] == {

        '_id': '5cf5a5765785504c8899cd29',
        '__v': 0,
        'email_confirmed': True,
        'accounts': [],
        'facilities': [{
            'user_type': 'guest',
            'facility_name': 'CVDs Facility Test',
            '_id': '5cf5aa095785504c8899cd7c'
        }],
        'resources': [{
            'process_name': 'Graphene Synthesis',
            'user_type': 'guest',
            'name': 'Test CVD',
            '_id': '5cf5a94f5785504c8899cd66',
            'camera_capable': True
        }],
        'profile': {
            'name': 'Test',
            'email': 'torosant@illinois.edu',
            'username': 'test',
            'password': '$2a$10$6DfzbHsKJE9HfuUzJ.Pgc.4H7rxro1y.UOus3jCvo/JbQ2VbBjdMu',
            'phone': '(217) 600-9690',
            'type': 'user',
            'address': {
                'street_address': '1206 West Green Street',
                'city': 'Urbana',
                'state': 'IL',
                'postal_code': '61801',
                'country': 'United States',
                'geo': [40.1111367, -88.2250555]
            }
        }
    }

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.get_user_by_id()
    assert str(e.value) == "'_id' is a required argument."

    # wrong user id:
    _id = '5cf5a5765785504c8899cd25'
    response = session.get_user_by_id(_id)
    assert response['success'] == False
    assert response['error'] == "failed to get user"
 
def test_register_new_user():
    
    # generate a random username:
    username = 'test_%s'% int(time.time())

    # new user
    new_user = {
        'name': 'Ricardo',
        'email': 'torosant@illinois.edu',
        'username': username,
        'phone': '217-6009692',
        'password': 'Test123*',
        'address': {
            'street_address': '1206 West Green Street',
            'city': 'Urbana',
            'state': 'IL',
            'postal_code': '61801',
            'country': 'United States'
        },
        'accounts': [
            {
                'number': None,
                'type': 'banner'
            }
        ]
    }

    session = OSCMClient(server_instance)
    
    # Test register user
    response_new_user = session.register_new_user(new_user)
    assert response_new_user['success'] == True
    
    # Test event
    current_event = {
        'event_type': 'activate-account',
        'user': response_new_user['user'],
        'user_email': new_user['email']
    }

    # add event into oscm
    response_event = session.add_event(current_event)
    assert response_event['success'] == True
    assert response_event['event']['event_type'] == 'activate-account'
    assert response_event['event']['user_email'] == new_user['email']
    assert response_event['event']['resolved'] == False

    # Test email confirmation

    is_email = session.send_confirmation_email(response_event['event']['_id'], new_user['email'])
    assert is_email['success'] == True 
    
    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.register_new_user()
    assert str(e.value) == "user is a required argument."

    with pytest.raises(TypeError) as e:
        session.add_event()
    assert str(e.value) == "current_event is a required argument."

    with pytest.raises(TypeError) as e:
        session.send_confirmation_email()
    assert str(e.value) == "event_id and email are required arguments."

# ---------------------------------------------------
# Resource:
# ---------------------------------------------------

def test_get_resource():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_resource("profile.phone")
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # resource id
    _id = '5cf5a94f5785504c8899cd66'
    response = session.get_resource(_id)
    assert response['success'] == True
    assert response['resource'] == {
        '_id': '5cf5a94f5785504c8899cd66',
        'owner': {
            '_id': '5c5c89c86c87be32f065714f',
            'name': 'Admin'
        },
        'configuration': {
            'profile': {
                'oscmkey': '$2a$10$7phVRXDhKUtg6kaLNEDd5OKx.xC54QGSbDFWJTTvmvubMpy2KgHVe',
                'license_number': '470',
                'resourcename': 'Test CVD',
                'camera_capable': True,
                'picture': 'uploaded',
                'description': 'testing ...'
            },
            'communications': {
                'adapters': [{
                    '_id': '5cf5a94f5785504c8899cd67',
                    'configuration': {
                        'name': 'Labview',
                        'id': '5015',
                        'adapter_type': 'RESOURCE',
                        'version': '0.5',
                        'stream_type': 'OSCM',
                        'stream_category': 'ALL',
                        'active': True
                    },
                    'locator': {
                        'port': 7171,
                        'octopi_address': 'NA',
                        'address': '127.0.0.1'
                    }
                }],
                'OSCM_cloud': {
                    'locator': {
                        'port': 8443,
                        'ip': '127.0.0.1',
                        'address': 'http://localhost:8001'
                    }
                }
            },
            'file': {
                'oscm': {
                    'User_defined': [],
                    'Base_pressure': '10',
                    'Sample_dimensions': {
                        'Diameter': '10',
                        'Length': '10',
                        'Thickness': '10',
                        'Surface_area': '10'
                    },
                    'Gases': [
                        {'gas': 'Hellium'},
                        {'gas': 'Hydrogen'},
                        {'gas': 'Carbon source'},
                        {'gas': 'Argon'}
                    ],
                    'catalyst': [
                        {'catalyst': 'Nickel'},
                        {'catalyst': 'Platinum'}
                    ],
                    'Furnace_dimension': {
                        'Tube_length': '10',
                        'Cross_sectional_area': '10',
                        'Tube_diameter': '10'
                    },
                    'manufacturer': {
                        'model': 'aaaa',
                        'make': 'AAA'},
                        'process_type': 'Atmospheric pressure CVD (APCVD) ',
                        'process_name': 'Graphene Synthesis',
                        'process_category': 'Chemical vapor deposition (CVD)'
                    }
                },
                'calendar': '5cf5a9515785504c8899cd68',
                'form': {
                    'formatArray': [
                        {'checked': True, 'display': 'Transaction name'},
                        {'checked': True, 'display': 'Quantity (number of parts)'},
                        {'checked': False, 'display': 'Accounts (class and banner accounts)'},
                        {'checked': True, 'display': 'Date and Time'},
                        {'checked': True, 'display': 'Files (let customers upload files)'},
                        {'checked': True, 'display': 'Instructions (let customers give more details)'},
                        {'checked': False, 'display': 'Other (customize your own questions)'}
                    ]
                }
            },
            'address': {
                'country': 'United States',
                'postal_code': '61801',
                'state': 'IL',
                'city': 'Urbana',
                'street_address': '1206 West Green Street'
            },
            'geo': [40.1111367, -88.2250555],
            'users': [{
                'user_type': 'guest',
                'name': 'Test',
                '_id': '5cf5a5765785504c8899cd29',
                'account': {
                    'type': 'class'
                }
            }
        ]
    }

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.get_resource()
    assert str(e.value) == "'_id' is a required argument."

    # wrong resource id:
    _id = '5cf5a5765785504c8899cd25'
    response = session.get_resource(_id)
    assert response['success'] == False
    assert response['error'] == "failed to get resource"

# ---------------------------------------------------
# Facility:
# ---------------------------------------------------

def test_get_facility():
    
    session = OSCMClient(server_instance)

    _id = '5cf5aa095785504c8899cd7c'
    path = '_doc'

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_facility(_id, path)
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # resource id
    response = session.get_facility(_id, path)
    assert response['success'] == True
    assert response['facility'] == {
        'owner_locator': {
            '_id': '5c5c89c86c87be32f065714f',
            'name': 'Admin'
        },
        'physical_locator': {
            'address': {
                'country': 'United States',
                'postal_code': '61801',
                'state': 'IL',
                'city': 'Urbana',
                'street_address': '1206 West Green Street'
            },
            'location_data': {
                'geo': [40.1111367, -88.2250555]
            }
        },
        'users_locator': [{
            'user_type': 'guest',
            'name': 'Test',
            '_id': '5cf5a5765785504c8899cd29',
            'account': {'type': 'class'}
        }],
        'resources_locator': [{
            '_id': '5cf5a94f5785504c8899cd66',
            'name': 'Test CVD'
        }],
        'configuration': {
            'profile': {
                'description': 'This is a test ....',
                'picture': 'uploaded',
                'facility_name': 'CVDs Facility Test',
                'license_number': '1618',
                'oscmkey': '$2a$10$LP4fsEgxksm6uu6XKUu.FutkTzdIjYmZDQjMVXESoFh7gdm4sgoEG'
            },
            'communications': {
                'OSCM_cloud': {
                    'locator': {
                        'port': 8443,
                        'ip': '127.0.0.1',
                        'address': 'http://localhost:8001'
                    }
                }
            },
            'calendar': '5cf5aa095785504c8899cd7e',
            'queues': [{
                'name': 'Test',
                '_id': '5cf5aa095785504c8899cd7d',
                'description': 'This is a test'
            }],
            'form': {
                'formatArray': [
                    {'checked': True, 'display': 'Transaction name'},
                    {'checked': True, 'display': 'Quantity (number of parts)'},
                    {'checked': False, 'display': 'Accounts (class and banner accounts)'},
                    {'checked': False, 'display': 'Date and Time'},
                    {'checked': True, 'display': 'Files (let customers upload files)'},
                    {'checked': True, 'display': 'Instructions (let customers give more details)'},
                    {'checked': False, 'display': 'Other (customize your own questions)'}
                ]
            }
        },
        'active': True,
        '__v': 0,
        '_id': '5cf5aa095785504c8899cd7c'
    }

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.get_facility()
    assert str(e.value) == "'_id' is a required argument."

    # wrong facility id:
    _id = '5cf5a5765785504c8899cd25'
    response = session.get_facility(_id, path)
    assert response['success'] == False
    assert response['error'] == "failed to get facility"

    # wrong path arg:
    _id = '5cf5aa095785504c8899cd7c'
    path = 'anything'
    with pytest.raises(ValueError) as e:
        session.get_facility(_id, path)
    assert str(e.value) == "'path' does not exist in facility model"

def test_get_file():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_files_metadata('5cf999e45785504c8899cda0')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # transaction id
    _id = '5cf999e45785504c8899cda0'
    response_files = session.get_files_metadata(_id)
    assert response_files['success'] == True

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.get_files_metadata()
    assert str(e.value) == "'_id' is a required argument."

    # wrong id arg:
    _id = '5cf5a5765785504c8899cd25'
    response = session.get_files_metadata(_id)
    assert response['success'] == False
    assert response['error'] == "failed to get file metadata"

# ---------------------------------------------------
# Transactions:
# ---------------------------------------------------

def test_get_transactions():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_transactions('customer', version='light')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # test only success. Transactions are changing everyday
    response_customer = session.get_transactions('customer', version='light')
    assert response_customer['success'] == True
    response_provider = session.get_transactions('provider', version='light')
    assert response_provider['success'] == True

def test_get_transaction():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_transaction('5cf999e45785504c8899cda0')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    transaction_id = '5cf999e45785504c8899cda0'
    response = session.get_transaction(transaction_id)
    assert response['success'] == True
    assert response['transaction'] == {
        '__v': 0,
        '_id': '5cf999e45785504c8899cda0',
        'messages': [],
        'properties': {
            'name': 'Test transaction customer',
            'account': None,
            'payment_method': None,
            'submitted': '2019-06-06T22:55:32.128Z',
            'status': 'requested'
        },
        'job_locator': {
            '_id': '5cf999e45785504c8899cd9f'
        },
        'resource_locator': {
            '_id': '5cf5a94f5785504c8899cd66',
            'name': 'Test CVD',
            'type': 'resource'
        },
        'customer_locator': {
            '_id': '5cf5a5765785504c8899cd29',
            'name': 'Test',
            'email': 'torosant@illinois.edu',
            'phone': '(217) 600-9690'},
            'provider_locator': {
                '_id': '5c5c89c86c87be32f065714f',
                'name': 'Admin',
                'email': 'torosant@illinois.edu'
            }
    }
    assert response['job'] == {
        '__v': 0,
        '_id': '5cf999e45785504c8899cd9f',
        'other': None,
        'instructions': 'no special instructions for this job',
        'quantity': 1,
        'times': {
            'processing': 2,
            'cleaning': 0
        },
        'dates': {
            'duedate': '2019-06-10T20:00:00.000Z',
            'readytime': '2019-06-10T18:00:00.000Z',
            'submitted': '2019-06-03T22:54:14.595Z',
            'end': ['2019-06-10T20:00:00.000Z'],
            'start': ['2019-06-10T18:00:00.000Z']
        },
        'properties': {
            'locked': True,
            'mode': 'manual',
            'type': 'single',
            'status': 'requested'
        },
        'users_locator': {
            'customer_id': '5cf5a5765785504c8899cd29',
            'resource_id': '5cf5a94f5785504c8899cd66'
        },
        'calendar_locator': {
            '_id': '5cf5a9515785504c8899cd68'
        }
    }

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.get_transaction()
    assert str(e.value) == "'_id' is a required argument."

    # wrong id arg:
    _id = '5cf999e45785504c8899cda1'
    response = session.get_transaction(_id)
    assert response['success'] == False
    assert response['error'] == "failed to get transaction"

def test_set_transaction():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_transaction('5cf999e45785504c8899cda0')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    # get user facilities:
    user_facilities = session.get_profile('facilities')['field']
    # select the first facility and get id:
    facility_id = user_facilities[0]['_id']
    # get facility
    user_facility = session.get_facility(facility_id)['facility']
    # get the first queue id 
    queue_id = user_facility['configuration']['queues'][0]['_id']
    # pre build job
    job_data = {
        'type': 'facility',
        'queue': queue_id,
        'start': None,
        'end': None,
        'processing': None,
        'quantity': 1,
        'instructions': None
    }

    # get customer locator
    customer_locator = {
        "_id": session.user["profile"]["id"],
        "name": session.user["profile"]["name"],
        "email": session.user["profile"]["email"],
        "phone": session.get_profile("profile.phone")['field']
    }

    # get facility info
    resource = session.get_facility(facility_id, path='_doc')['facility']

    # get provider info and set provider info for future use
    provider_id = resource['owner_locator']['_id']
    if customer_locator['_id'] is provider_id:
        status = 'approved'
        provider_locator = {
            '_id': customer_locator['_id'],
            'name': customer_locator['name'],
            'email': customer_locator['email']
        }
    else:
        status = 'requested'
        provider = session.get_user_by_id(provider_id)['user']
        provider_locator = {
            '_id': provider['_id'],
            'name': provider['profile']['name'],
            'email': provider['profile']['email']
    }

    # ------------
    # Job:
    # ------------

    # set calendar id
    calendar_locator = {
        '_id': resource['configuration']['calendar']
    }
    
    # set user_locator
    users_locator = {
        'customer_id': customer_locator['_id'],
        'resource_id': facility_id
    }

    # set properties (for now we are assumed to be manual mode)
    job_properties = {
        'status': status,
        'type': 'single',
        'mode': 'queue',
        'locked': False
    }
    
    # set dates
    start = job_data['start']
    end = job_data['end']

    dates = {
        'start': [start],
        'end': [end],
        'duedate': end,
        'readytime': start
    }

    # set times
    times = {
        'processing': job_data['processing'],
        'setuptime': 0,
        'cleaning': 0
    }

    # build job
    job = {
        'calendar_locator': calendar_locator,
        'users_locator': users_locator,
        'properties': job_properties,
        'dates': dates,
        'times': times,
        'quantity': job_data['quantity'],
        'instructions': 'no special instructions for this job',
        'other': None,
    }

    # ------------
    # Transaction:
    # ------------

    # set resource locator
    resource_locator = {
        '_id': facility_id,
        'name': resource['configuration']['profile']['facility_name'],
        'type': job_data['type'],
        'queue': job_data['queue']
    }

    # set job locator (for now we assumed to be None)
    job_locator = {
        '_id': None
    }

    # set transaction properties
    transaction_properties = {
        'name': 'testing transaction',
        'status': status
    }

    # messages (please empty list)
    messages = []

    # build transaction profile
    transaction = {
        'provider_locator': provider_locator,
        'customer_locator': customer_locator,
        'resource_locator': resource_locator,
        'job_locator': job_locator,
        'properties': transaction_properties,
        'messages': messages,
    }

    # ------------
    # Set transaction:
    # ------------

    response = session.submit_transaction(job, transaction)
    assert response['success'] == True

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.submit_transaction()
    assert str(e.value) == "job and transaction are required arguments."

    # wrong args:
    response = session.submit_transaction('anything', 'anything')
    assert response['success'] == False
    assert response['error'] == "failed to submit transaction"

def test_send_email():
    
    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.send_email('receiver', 'subject', 'body')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    receiver = session.user["profile"]["email"]
    subject = 'Running test'
    body = '<p> This is a test... </p>'
    response = session.send_email(receiver, subject, body)
    assert response['success'] == True

    # wrong args:
    response = session.send_email('anything', 'anything', 'anything')
    assert response['success'] == False
    assert response['error'] == "failed to send email" 


def test_submit_file():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.submit_file('transaction_data', 'oscm_path', 'filename')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    transaction_id = '5cf999e45785504c8899cda0'
    current_transaction = session.get_transaction(transaction_id)['transaction']

    transaction_data = {
        'user_id': current_transaction['customer_locator']['_id'],
        'transaction_id': current_transaction['_id'],
        'provider_id': current_transaction['provider_locator']['_id']
    }

    oscm_dir = 'oscm_files'
    oscm_path = os.path.abspath(oscm_dir)

    filename = 'recipe2.json'

    # create fake JSON data
    response_dict = {
        'json': {
            'name': 'Ricardo',
            'last': 'Toro'
        }
    }

    # create file
    dump_file = open(os.path.join(oscm_path, filename), 'w')
    json.dump(response_dict['json'],dump_file)
    dump_file.close()
    
    # Submit file with metadata
    response = session.submit_file(transaction_data, oscm_path, filename)
    assert response['success'] == True
    assert response['filename'] == 'recipe2.json'

    # random args:
    with pytest.raises(TypeError) as e:
        session.submit_file('anything', 'anything', 'anything')
    assert str(e.value) == "Transaction argument must be a dict."


def test_get_file():

    session = OSCMClient(server_instance)

    # test no auth
    with pytest.raises(ValueError) as e:
        session.get_file('5cf999e45785504c8899cda0')
    assert str(e.value) == "'token' expired or user never authenticated"

    # Auth
    session.authenticate(username, password)

    transaction_id = '5cf999e45785504c8899cda0'
    files = session.get_files_metadata(transaction_id)
    assert files['success'] == True
    
    file_id = files['files'][0]['id']
    file = session.get_file(file_id)
    assert file['success'] == True
    assert file['filename'] == 'recipe2.json'

    # Missing arguments:
    with pytest.raises(TypeError) as e:
        session.get_file()
    assert str(e.value) == "'_id' is a required argument."

    # wrong id arg:
    _id = '5cf5a5765785504c8899cd25'
    response = session.get_file(_id)
    assert response['success'] == False
    assert response['error'] == "failed to get file"
 